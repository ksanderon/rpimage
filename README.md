# RaspberryPi Arch Linux image generator

This project will not provide any code. Here I just want to use docker shared runners and gitlab ci to produce ready to use arch linux raspberry pi images based on packages provided on [http://os.archlinuxarm.org/os/ArchLinuxARM-rpi-2-latest.tar.gz](http://os.archlinuxarm.org/os/ArchLinuxARM-rpi-2-latest.tar.gz)

### [Download](https://gitlab.com/ksanderon/rpimage/builds)